package fr.formation.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.PreparedStatement;

import fr.formation.buisness.beans.Formation;
import fr.formation.buisness.beans.PersonneDao;



public class DaoImpl implements IDao{
	
	
	 private static DaoImpl dao = null;
	 private Connection connection;

	 public static DaoImpl getInstance(){
			
			if(dao==null){
				dao = new DaoImpl();
			}
			
			return dao;
		}

	@Override
	public  PersonneDao createPersonne(PersonneDao personne) throws SQLException {
		PreparedStatement preparedStatement=null;
		ResultSet rs = null ;
		connection =  ConnectionUtil.getInstance().getConnection();
		 
			try {
				
				//connection = DBUtilWithDataSource.getConnection()
				//connection =  ConnectionUtil.getInstance().getConnection();
				preparedStatement = (PreparedStatement) connection.prepareStatement("insert into PERSONNE(NOM,PRENOM,EMAIL,MDP) values (?, ?, ?, ? )",Statement.RETURN_GENERATED_KEYS);
	            // Parameters start with 1
	            preparedStatement.setString(1, personne.getNom());
	            preparedStatement.setString(2, personne.getPrenom());
	            preparedStatement.setString (3, personne.getEmail());
	            preparedStatement.setString (4, personne.getMdp());
	            preparedStatement.executeUpdate();
	           
	            int id =0;
	             rs = preparedStatement.getGeneratedKeys();
	            
	            		 
	            if (rs != null && rs.next()) {
	                id = rs.getInt(1);
	            }
	            System.out.println(" lid genere " + id );
	            personne.setId(id);
	            rs.close();
	            preparedStatement.close();
	            connection.close();
	           
			} catch (SQLException e) {
	            throw new DaoException(e.getMessage(), 1);
	        }
			return personne;
		
	}
	
	public  List<PersonneDao> getAllPersonnes() throws SQLException{
		List<PersonneDao> liste = new ArrayList<PersonneDao>();
		  ResultSet rs = null ;
		  PreparedStatement preparedStatement = null;
		  	connection =  ConnectionUtil.getInstance().getConnection();
		  try {	
			  	preparedStatement = (PreparedStatement) connection.prepareStatement("select * from personne");
	             rs = preparedStatement.executeQuery();
	           
			 
	            while (rs.next()) {
	            PersonneDao user = new PersonneDao();
	                user.setId (rs.getInt("id"));
	                user.setNom (rs.getString("nom"));
	                user.setPrenom(rs.getString("prenom"));
	                user.setEmail(rs.getString("email"));
	            
	                liste.add(user);
	               
	            }
	            
	            rs.close();
	            preparedStatement.close();
	            connection.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		return liste;
		
	}

	@Override
	public PersonneDao findPersonneByUtilisateur(PersonneDao p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Formation> getListeFormationParPersonne(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void creationFormation(Formation f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Formation> recupererAllFormation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void inscriptionFormation(int idPersonne, int idFormation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Formation> getListeFormationParFormateur(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}