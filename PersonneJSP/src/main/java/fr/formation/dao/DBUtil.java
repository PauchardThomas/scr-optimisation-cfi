package fr.formation.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

	private static Connection connection = null;

	public static Connection getConnection() {
		if (connection != null)
			return connection;
		else {
			try {
				// on doit externaliser ces donn�es

				// Properties prop = new Properties();
				// OutputStream inputStream = new
				// FileOutputStream("datasource.properties");
				// InputStream inputStream =
				// DBUtil.class.getClassLoader().getResourceAsStream("datasource.properties");
				// prop.load(inputStream);
				// String driver = prop.getProperty("driver");
				// String url = prop.getProperty("url");
				// String user = prop.getProperty("user");
				// String password = prop.getProperty("password");
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager
						.getConnection(
								"jdbc:mysql://localhost:3306/base_personne",
								"root", "root"); // pas de mdp sous windows
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			//
			return connection;
		}

	}
}
