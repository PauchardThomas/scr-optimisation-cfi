package fr.optim.bestpractice;

public class Duplicate {
	
	public static void main(String[] args) {
		 int [] array = new int [101];
		 for(int i  = 0 ; i<=100;i++){
			 array[i]=i;
		 }
		 System.out.println(array[100]);

		 
		 //la bonne facon de copier car fait appel a du code natif . 
		 final int[] copy = new int [array.length];
		    System.arraycopy(array, 0, copy, 0, array.length);
		    System.out.println(copy[100]);
		    
		//mauvaise assignation
		    for(int i  = 0 ; i<copy.length;i++){
				 copy[i]= copy[i] +1;
			 }  
		    System.out.println(copy[100]);
		    
		 //la bonne facon d'assign� 
		    for(int i  = 0 ; i<array.length;i++){
		    	array[i] +=1;
			 }  
		    System.out.println(array[100]);
	}

}
