package fr.optim.bestpractice;

public class Concatenation {
	public static void main(String[] args) throws InterruptedException {
		
		testString();
		//testStringBuffer();
		//testStringBuilder();
//		
//		String mot = "coucou ";
//		mot +=" tout le monde";
//		mot+="je vous souhaite";
//		mot+="une bonne formation";
//		
//		System.out.println(mot);
//		
//		 final StringBuilder string = new StringBuilder("tout le monde");
//		    string.append("je vous souhaite ");
//		    string.append(" une bonne formation");
//
//		    System.out.println(string);
		
	}
	
	
	public static void testString() throws InterruptedException{
		//Thread.sleep(1000);
		
		String s = "testString";
		for (int i = 0; i < 100000; i++) {
			s = s + "AjoutTestString";
			if(i==99999){
				System.out.println("faire un dump ");
				Thread.sleep(5000);
				
				
			}
		}
		
	}
	
	public static void testStringBuffer() throws InterruptedException{
		Thread.sleep(1000);
		StringBuffer s = new StringBuffer("testStringBuffer");
		for (int i = 0; i < 100000; i++) {
			
			s.append("ajoutBuffer");
			if(i==99999){
				System.out.println("faire un dump ");
				Thread.sleep(5000);
				
				
			}
		}
	}
	
	public static void testStringBuilder() throws InterruptedException{
		Thread.sleep(1000);
		StringBuilder s = new StringBuilder(5000);
		s.append("testStringBuilder");
		for (int i = 0; i < 100000; i++) {
			s.append("ajoutBuffer");
			if(i==99999){
				System.out.println("faire un dump ");
				Thread.sleep(10000);
				
				
			}
		}
	}

}
