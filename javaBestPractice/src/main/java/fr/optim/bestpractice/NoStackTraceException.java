package fr.optim.bestpractice;

public class NoStackTraceException extends Exception {

	public NoStackTraceException(String message) {
		super(message);
	}

	public synchronized Throwable fillInStackTrace() {
		// on ne fait rien
		return this;
	}
}
