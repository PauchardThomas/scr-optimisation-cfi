package fr.optim.bestpractice;

public class PerfException {
	
	public int methodReturnInt (String param) {
		if (param==null) {
			return -1;
		}
		// traitement
		return 0;
	}

	public void methodThrowsException (String param) throws Exception {
		if (param==null) {
			//throw new Exception("La valeur 'null' n'est pas accept�e");
			throw new NoStackTraceException("no stack");
		}
		// traitement
	}
	
	public void testPerf (String param, int iteration) {
		long start;
		long duree;

		System.out.println(" * " + iteration + " appels de methode avec '"
			+ param + "': ");

		System.out.print("\t methodReturnInt        : ");
		
		start = System.currentTimeMillis();
		for (int i=0; i<iteration; i++) {
			switch (methodReturnInt(param)) {
				case -1:
					// Traitement Erreur
					break; 
				case 0:
					// Traitement OK
				break;
			}
		}
		duree = System.currentTimeMillis()-start;
		System.out.println( duree + " ms.");


		System.out.print("\t methodThrowsException  : ");
		start = System.currentTimeMillis();
		for (int i=0; i<iteration; i++) {
			try {
				methodThrowsException(param);
				// Traitement OK
			} catch (Exception e){
				// Traitement Erreur
			}
		}
		duree = System.currentTimeMillis()-start;
		System.out.println( duree + " ms.");
		System.out.println();
	}

	
	public static void main(String[] args) {
		PerfException obj = new PerfException();

		int iteration = 5000000;
		
		obj.testPerf("string", iteration);
		obj.testPerf(null, iteration);
	}


}
