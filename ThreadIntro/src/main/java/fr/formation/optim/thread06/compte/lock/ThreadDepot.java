package fr.formation.optim.thread06.compte.lock;

import java.util.Random;

public class ThreadDepot extends Thread{
   
   private Compte ceb;
   private Random rand = new Random();
   
   public ThreadDepot(Compte c){
      ceb = c;
      this.setName("D�p�t");
   }
   
   public void run() {
       while(true){
          
          int nb = rand.nextInt(100);
          long montant = Integer.valueOf(nb).longValue();
          ceb.depot(montant);
          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {}
       }
   }
}
