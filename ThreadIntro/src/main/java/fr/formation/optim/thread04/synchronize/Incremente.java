package fr.formation.optim.thread04.synchronize;


public class Incremente {
   private int entier = 0;
  
   public synchronized void incremente(){
    entier++;
   }
   public Integer get(){
	   synchronized(this){
      return entier;
	   }
   }
}
