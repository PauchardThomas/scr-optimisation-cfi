package fr.formation.optim.thread03.atomique;

import java.util.concurrent.atomic.AtomicInteger;

public class TestProcessusThread {
	public static AtomicInteger entier = new AtomicInteger();

	public static void main(String[] args) {

		Thread t1 = new Thread(new Test());
		Thread t2 = new Thread(new Test());
		Thread t3 = new Thread(new Test());
		Thread t4 = new Thread(new Test());

		Thread t5 =new Thread( new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						System.out.println("threadBis coucou");
						Thread.sleep(5000);
					} catch (InterruptedException e) {

					}
				}

			}
		});
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		t5.start();
		
		
	}
}
