package fr.formation.optim.thread03A.atomique.cancel;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * @author CADJEE Ismael 
 *
 */
public class TestProcessusThread {
	   public static AtomicInteger entier = new AtomicInteger();
	   public static void main(String[] args) {  
	      
	      
	      Thread t1 = new Thread(new Compteur(10)," Max dix");
	      Thread t2 = new Thread(new Compteur(2),"Max deux");
	      Thread t3 = new Thread(new Compteur(30), "Max trente");
	      Thread t4 = new Thread(new Compteur(0),"Max zero");
	      
	      t1.start();
	      t2.start();
	      t3.start();
	      t4.start();
	   }   
	}
