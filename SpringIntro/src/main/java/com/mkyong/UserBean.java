package com.mkyong;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.context.annotation.Scope;
import com.mkyong.user.bo.UserBo;
import com.mkyong.user.bo.UserBoNoSpring;
import com.mkyong.user.bo.impl.UserBoNoSpringImpl;

@Named
@Scope("request")
public class UserBean {

	@Inject
	UserBo userBo;
	
	UserBoNoSpring userBoNoSpring = new UserBoNoSpringImpl();

	public void setUserBo(UserBo userBo) {
		this.userBo = userBo;
	}

	public String printMsgFromSpring() {
		return userBo.getMessage()+ " "+ userBo.toString();
	}

	public String printMsgNoFromSpring() {
		return userBoNoSpring.getMessage()+ " "+ userBoNoSpring.toString();
	}
}